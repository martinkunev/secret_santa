#!/usr/bin/python3

import random
import sys
import smtplib, ssl
import json
from getpass import getpass

smtp_host = "smtp.gmail.com"
smtp_port = 465

def select(people):
	def select_name(selected=[]):
		index = len(selected)

		if index == len(names):
			return {people[p]: s for p, s in zip(names, selected)}

		remaining = list(set(names) - set(selected))
		random.shuffle(remaining)
		for r in remaining:
			if names[index] == r:
				continue
			result = select_name(selected + [r])
			if result != None:
				return result

		return None

	names = list(people.keys())
	return select_name()

def send(selected):
	sender = "martinkunev@gmail.com"
	password = getpass("Password for {}: ".format(sender))

	template = \
"""From: {}
To: {}
Subject: Secret Santa

Tu es le secret santa de {}"""

	context = ssl.create_default_context()
	with smtplib.SMTP_SSL(smtp_host, smtp_port, context=context) as client:
		client.login(sender, password)

		for receiver, target in selected.items():
			message = template.format(sender, receiver, target)
			client.sendmail(sender, receiver, message)

if len(sys.argv) == 2:
	people = None
	with open(sys.argv[1], "r") as f:
		people = json.load(f)
	selected = select(people)
	with open("targets.json", "w") as f:
		json.dump(selected, f)
	send(selected)
elif len(sys.argv) == 1:
	selected = None
	with open("targets.json") as f:
		selected = json.load(f)
	print(selected)
	send(selected)
else:
	print("usage: {} [name_email_json]".format(sys.argv[0]))
