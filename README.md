# secret_santa

Specify names of people and emails as JSON (see `people.json` for an example). Run the script with the specified file:

	python3 secret_santa.py <filename>

Each person would be assigned a secret santa from the file (no person will be the secret santa of themselves). Each person will receive an email indicating who their secret santa is. The assignments should remain secret but a backup will be stored in `target.json`. If you want to resend the emails for the previous assignment, run the script without arguments:

	python3 secret_santa.py

Adapted for gmail (you need 2-factor authentication and app password enabled to use it with gmail).
